Architektura
==================
Diagram klas
------------------
.. uml::


	class Customer {
	int id
	String firstName
	String lastName
	  Customer()
	  Customer(firstName : String, lastName : String)
	  Long getId()
	  String getFirstName()
	  String getLastName()
	  String toString()
	}

	interface CustomerRepository {
		List <Customer>findByLastName(lastName : String)
		 Customer findById(id : Long)
	}
	class CustomerController{
		CustomerRepository customerRepository
		Iterable<Customer> getCustomers()
		Customer getUserById(id : Long)
	}
	CustomerController <|-- CustomerRepository
	CustomerRepository o-- Customer


Diagram przypadków użycia
-------------------------

.. uml::

	left to right direction
	actor "User" as fc
	actor "Database" as db

	rectangle App{
	usecase "Get customers" as UC1
	usecase "Get customer by ID" as UC2
	}
	fc --> UC1
	fc --> UC2
	UC1 <-- db
	UC2 <-- db
