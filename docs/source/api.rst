.. toctree::
   :maxdepth: 2
   :caption: Contents:



REST api
==================
Definicja
------------------
REST api jest to system pozwalający na komunikację między klientem, a serwerem za pomocą protokołu http. 

Endpointy
------------------
- GET .../hello - otwiera stronę 'Hello Docker World'
- GET .../customers/{id} - pobranie klienta o określonym id
- GET .../customers - pobranie wszystkich klientów

