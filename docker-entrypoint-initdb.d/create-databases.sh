#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER dockerDB with encrypted password 'password';
    CREATE DATABASE docker;
    GRANT ALL PRIVILEGES ON DATABASE docker TO dockerDB;
EOSQL